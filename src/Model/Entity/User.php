<?php

namespace App\Model\Entity;

use Cake\Auth\PasswordHasherFactory;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property bool $active
 * @property bool $is_superuser
 * @property string $role
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $deleted
 * @property array $settings
 * @property string $token
 * @property \Cake\I18n\FrozenTime $token_expires
 * @property string $api_token
 * @property \Cake\I18n\FrozenTime $tos_date
 * @property string $activation_secret
 * @property \Cake\I18n\FrozenTime $activation_date
 * @property string $reset_password_secret
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        'email' => true,
        'password' => true,
        'first_name' => true,
        'last_name' => true,
        'active' => true,
        'is_superuser' => false,
        'role' => false,
        'created' => false,
        'modified' => false,
        'deleted' => false,
        'settings' => true,
        'token' => false,
        'token_expires' => false,
        'api_token' => false,
        'tos_date' => false,
        'activation_secret' => false,
        'activation_date' => false,
        'reset_password_secret' => false,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
        'secret',
    ];

    /**
     * Virtual fields
     */
    protected $_virtual = [
        'token_expired'
    ];

    /**
     * @param string $password password that will be set.
     * @return bool|string
     */
    protected function _setPassword($password)
    {
        return $this->hashPassword($password);
    }

    /**
     * @param string $tos tos option. It will be set the tos_date
     * @return bool
     */
    protected function _getTokenExpired($tos)
    {
        return $this->tokenExpired();
    }

    /**
     * @param string $tos tos option. It will be set the tos_date
     * @return bool
     */
    protected function _setTos($tos)
    {
        if ((bool)$tos === true) {
            $this->set('tos_date', FrozenTime::now());
        }

        return $tos;
    }

    /**
     * Checks if a password is correctly hashed
     *
     * @param string $password password that will be check.
     * @return bool
     */
    public function checkPassword($password)
    {
        $PasswordHasher = $this->getPasswordHasher();

        return $PasswordHasher->check($password, $this->password);
    }

    /**
     * Return the configured Password Hasher
     *
     * @return \Cake\Auth\AbstractPasswordHasher Password hasher instance
     */
    public static function getPasswordHasher()
    {
        $passwordHasher = Configure::read('Auth.passwordHasher', 'Default');

        return PasswordHasherFactory::build($passwordHasher);
    }

    /**
     * Hash a password using the configured password hasher,
     * use DefaultPasswordHasher if no one was configured
     *
     * @param string $password password to be hashed
     * @return mixed
     */
    public function hashPassword($password)
    {
        $PasswordHasher = $this->getPasswordHasher();

        return $PasswordHasher->hash($password);
    }

    /**
     * Returns if the token has already expired
     *
     * @return bool
     */
    public function tokenExpired()
    {
        if (empty($this->token_expires)) {
            return true;
        }

        return new FrozenTime($this->token_expires) < FrozenTime::now();
    }
}
