<?php

namespace App\Controller;

use Cake\Chronos\Chronos;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Auth Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class AuthController extends AppController
{
    /**
     * The base model class
     * @var string
     */
    public $modelClass = 'Users';

    /**
     * Initialize method
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    /**
     * Before Filter event
     * @param \Cake\Event\Event $event The event
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $this->Crud->mapAction('register', 'Crud.Add');
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        $identify = $this->Auth->identify();
        if (!$identify) {
            throw new UnauthorizedException('Invalid username or password');
        }

        $user = $this->Users->get($identify['id']);
        $user->token_expires = Chronos::parse('+7 hours');
        $user->token = JWT::encode(
            [
                'sub' => $identify['id'],
                'exp' => $user->token_expires->timestamp
            ],
            Security::getSalt()
        );
        $user = $this->Users->save($user);

        $this->set([
            'success' => true,
            'data' => [
                'id' => $user->id,
                'token' => $user->token
            ],
            '_serialize' => ['success', 'data']
        ]);
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|void
     */
    public function register()
    {
        $this->Crud->on('beforeSave', function (Event $event) {
            /* @var \App\Model\Entity\User $user */
            $user = $event->subject->entity;
            $user->active = false;
            $user->activation_secret = bin2hex(Security::randomBytes(16));
            $user->token_expires = Chronos::parse('+7 hours');
            $user->token = JWT::encode(
                [
                    'sub' => $user->id,
                    'exp' => $user->token_expires->timestamp
                ],
                Security::getSalt()
            );
        });

        $this->Crud->on('afterSave', function (Event $event) {
            if ($event->subject->created) {
                /* @var \App\Model\Entity\User $user */
                $user = $event->subject->entity;
                $this->set('data', [
                    'id' => $user->id,
                    'token' => $user->token
                ]);
                $this->Crud->action()->setConfig('serialize.data', 'data');
            }
        });

        return $this->Crud->execute();
    }

    /**
     * Register method
     *
     * @param string $id the user id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Network\Exception\BadRequestException When query parameter check is not passed
     * @throws \Cake\Core\Exception\Exception When validation token is wrong
     * @throws \Cake\Network\Exception\UnauthorizedException When validation secret is wrong
     */
    public function verifyAccount(string $id)
    {
        $check = $this->request->getQuery('check');
        if (!$check) {
            throw new BadRequestException(__('Missing validation token'));
        }

        $user = $this->Users->get($id);
        if ($user->activation_date) {
            $this->set([
                'success' => true,
                'message' => __('User is already verified'),
                '_serialize' => ['success', 'message']
            ]);

            return;
        }

        if ($user->activation_secret !== $check) {
            throw new Exception('Failed to verify token');
        }

        $user->active = true;
        $user->activation_date = Chronos::now();
        $user->activation_secret = null;
        $this->Users->save($user);

        $this->set([
            'success' => true,
            'message' => __('User verified successfully'),
            '_serialize' => ['success', 'message']
        ]);
    }

    /**
     * Reset Password method
     *
     * @return \Cake\Http\Response|void
     */
    public function resetPassword()
    {
        $email = $this->request->getData('email');
        if (!$email) {
            throw new BadRequestException(__('Missing email'));
        }

        /* @var \App\Model\Entity\User $user */
        $user = $this->Users->find()->where(['email' => $email])->first();
        if (!$user) {
            throw new Exception(__('Unknow email'));
        }

        $user->reset_password_secret = bin2hex(Security::randomBytes(16));
        $this->Users->save($user);

        //TODO: send email with $user->reset_password_secret

        $this->set([
            'success' => true,
            'message' => __('Reset password token sent to your email'),
            '_serialize' => ['success', 'message']
        ]);
    }

    /**
     * Change Password method
     *
     * @param string $id The user email
     * @return \Cake\Http\Response|void
     * @throws \Cake\Network\Exception\BadRequestException When query parameter check is not passed
     * @throws \Cake\Core\Exception\Exception When validation token is wrong
     */
    public function changePassword(string $id)
    {
        $check = $this->request->getQuery('check');
        if (!$check) {
            throw new BadRequestException(__('Missing validation token'));
        }

        $user = $this->Users->get($id);
        if (!$user->reset_password_secret || $user->reset_password_secret !== $check) {
            throw new Exception('Failed to reset password');
        }

        $user->reset_password_secret = null;
        $user->password = $this->request->getData('password');
        $this->Users->save($user);

        //TODO: warn user about password change

        $this->set([
            'success' => true,
            'message' => __('User\'s password changed successfully'),
            '_serialize' => ['success', 'message']
        ]);
    }
}
