<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Status Controller
 *
 */
class StatusController extends AppController
{

    /**
     * Initialize method
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        Configure::write('API.maintenance', true);
        $apiStatus = Configure::readOrFail('Api');
        $status = [
            'status' => $apiStatus['maintenance'] ? 'MAINTENANCE' : 'OK',
            'pages' => $apiStatus['endpoints']
        ];
        $this->set(compact('status'));

        $this->set([
            'status' => $status,
            '_serialize' => ['status']
        ]);

        return null;
    }
}
