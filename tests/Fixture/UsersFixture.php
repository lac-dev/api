<?php

namespace App\Test\Fixture;

use App\Model\Entity\User;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'email' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'password' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'first_name' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'last_name' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'default' => 0, 'null' => false, 'comment' => null, 'precision' => null],
        'is_superuser' => ['type' => 'boolean', 'length' => null, 'default' => 0, 'null' => false, 'comment' => null, 'precision' => null],
        'role' => ['type' => 'text', 'length' => null, 'default' => 'user', 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => 'now()', 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'deleted' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'settings' => ['type' => 'json', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'token' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'token_expires' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'api_token' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'tos_date' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'activation_secret' => ['type' => 'string', 'length' => 32, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'activation_date' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'reset_password_secret' => ['type' => 'string', 'length' => 32, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'users_email' => ['type' => 'unique', 'columns' => ['email'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public function init()
    {
        $passwordHasher = User::getPasswordHasher();

        $this->records = [
            [
                'id' => 'c52f7a14-2210-4c9b-8220-91665ee1009a',
                'email' => 'admin@dev.localhost',
                'password' => $passwordHasher->hash('admin'),
                'first_name' => 'Administrator',
                'last_name' => 'System',
                'active' => true,
                'is_superuser' => true,
                'role' => 'admin',
                'created' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'modified' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'deleted' => null,
                'settings' => '{}',
                'token' => null,
                'token_expires' => null,
                'api_token' => null,
                'tos_date' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'activation_secret' => null,
                'activation_date' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'reset_password_secret' => null
            ],
            [
                'id' => '51fed113-bd86-4a7b-886c-92f65a90e8a2',
                'email' => 'staff@dev.localhost',
                'password' => $passwordHasher->hash('tester'),
                'first_name' => 'Tester',
                'last_name' => 'System',
                'active' => false,
                'is_superuser' => false,
                'role' => 'user',
                'created' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'modified' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'deleted' => null,
                'settings' => '{}',
                'token' => null,
                'token_expires' => null,
                'api_token' => null,
                'tos_date' => null,
                'activation_secret' => null,
                'activation_date' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'reset_password_secret' => '726db601b6e91146af977a0a151f9ffc'
            ],
            [
                'id' => '8765579a-e861-4fda-8045-3fa3b95abc82',
                'email' => 'unactived_user@dev.localhost',
                'password' => $passwordHasher->hash('user'),
                'first_name' => 'Default',
                'last_name' => 'User',
                'active' => false,
                'is_superuser' => false,
                'role' => 'user',
                'created' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'modified' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'deleted' => null,
                'settings' => '{}',
                'token' => null,
                'token_expires' => null,
                'api_token' => null,
                'tos_date' => null,
                'activation_secret' => '513381bc9ea88dced32ba8be046115b9',
                'activation_date' => null,
                'reset_password_secret' => null
            ],
            [
                'id' => 'ed3e8045-7255-490d-83e4-27f511884069',
                'email' => 'deleted@dev.localhost',
                'password' => $passwordHasher->hash('deleted'),
                'first_name' => 'Deleted',
                'last_name' => 'System',
                'active' => false,
                'is_superuser' => false,
                'role' => 'admin',
                'created' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'modified' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'deleted' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'settings' => '{}',
                'token' => null,
                'token_expires' => null,
                'api_token' => null,
                'tos_date' => null,
                'activation_secret' => null,
                'activation_date' => date('Y-m-d H:i:s', time() - (60 * 60 * 24)),
                'reset_password_secret' => null
            ],
        ];

        parent::init();
    }
}
