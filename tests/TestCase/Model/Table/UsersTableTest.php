<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => UsersTable::class];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->assertEquals('id', $this->Users->getPrimaryKey());
        $this->assertEquals('email', $this->Users->getDisplayField());
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $validator = $this->Users->getValidator('default');

        $this->assertInstanceOf('Cake\Validation\Validator', $validator);
        $this->assertEquals(17, $validator->count());

        $newUser = [
            'email' => 'admin@dev.localhost',
            'password' => 'admin',
            'first_name' => 'Same Administrator',
            'last_name' => 'System',
            'active' => true,
        ];
        $errors = $validator->errors($newUser);

        $this->assertArrayHasKey('email', $errors);
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testCheckRules()
    {
        $builder = $this->Users->rulesChecker();

        $newUser = $this->Users->newEntity([
            'email' => 'admin@dev.localhost',
            'password' => 'admin',
            'first_name' => 'Same Administrator',
            'last_name' => 'System',
            'active' => true,
        ], ['validate' => false]);
        $this->assertInstanceOf('Cake\Datasource\RulesChecker', $builder);

        $result = $builder->checkCreate($newUser);

        $this->assertEquals(false, $result);
        $this->assertArrayHasKey('_isUnique', $newUser->getError('email'));
    }

    /**
     * Test find
     */
    public function testFindSuperusers()
    {
        $query = $this->Users->find('superusers');

        $this->assertInstanceOf('Cake\ORM\Query', $query);
        $result = $query->enableHydration(false)->toArray();

        $this->assertEquals(1, count($result));
    }
}
