<?php

namespace App\Test\TestCase\Model\Entity;


use App\Model\Entity\User;
use Cake\Chronos\Chronos;
use Cake\I18n\FrozenTime;
use Cake\TestSuite\TestCase;

/**
 * Class UserTest
 * @package App\Test\TestCase\Model\Entity
 */
class UserTest extends TestCase
{
    /**
     * Assert not all fields are visible
     */
    public function testVisibleFields()
    {
        $user = new User();
        $fields = $user->visibleProperties();

        $this->assertNotContains('password', $fields);
        $this->assertNotContains('token', $fields);
        $this->assertNotContains('secret', $fields);
    }

    /**
     * Assert password will not return unhashed
     */
    public function testHashedPassword()
    {
        $password = 'secret';
        $user = new User(['password' => $password]);

        $this->assertNotEquals($password, $user->password);
        $this->assertTrue($user->checkPassword($password));
    }

    /**
     *
     */
    public function testPasswordHasher()
    {
        $user = new User();
        $hasher = $user->getPasswordHasher();
        $password = 'secret';
        $hashedPassword = $user->hashPassword($password);

        $this->assertNotEquals($hashedPassword, $password);
        $this->assertInstanceOf('\Cake\Auth\AbstractPasswordHasher', $hasher);
    }

    /**
     *
     */
    public function testTosDate()
    {
        $user = new User(['tos' => true]);

        $this->assertInstanceOf('\DateTimeInterface', $user->tos_date);
    }

    /**
     *
     */
    public function testTokenExpiration()
    {
        Chronos::setTestNow(Chronos::now());

        $user = new User();
        $this->assertTrue($user->tokenExpired());

        $user = new User(['token_expires' => Chronos::parse('-1 second')]);
        $this->assertTrue($user->tokenExpired());
        $this->assertTrue($user->get('token_expired'));

        $user = new User(['token_expires' => Chronos::parse('+1 second')]);
        $this->assertFalse($user->tokenExpired());
        $this->assertFalse($user->get('token_expired'));
    }

}
