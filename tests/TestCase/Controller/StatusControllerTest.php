<?php

namespace App\Test\TestCase\Controller;

use App\Controller\StatusController;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StatusController Test Case
 */
class StatusControllerTest extends IntegrationTestCase
{

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->get('/status');
        $this->assertResponseOk();
    }

    public function testCheckStatusMaintenance()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
