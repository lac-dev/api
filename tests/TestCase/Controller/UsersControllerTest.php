<?php

namespace App\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $token = JWT::encode(
            [
                'sub' => 'c52f7a14-2210-4c9b-8220-91665ee1009a',
                'exp' => time() + 60
            ],
            Security::getSalt()
        );
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'authorization' => 'Bearer ' . $token
            ]
        ]);
    }

    /**
     * Test unauthenticated method
     *
     * @return void
     */
    public function testUnauthenticated()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
            ]
        ]);
        $this->get('/users');
        $this->assertResponseError();
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/users');
        $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/users/c52f7a14-2210-4c9b-8220-91665ee1009a');
        $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $data = [
            'email' => 'test@dev.localhost',
            'password' => 'tester',
            'first_name' => 'Test',
            'active' => true,
        ];

        $this->post('/users', $data);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['email' => $data['email']]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $id = '51fed113-bd86-4a7b-886c-92f65a90e8a2';
        $data = [
            'first_name' => 'Test',
        ];

        $this->put('/users/' . $id, $data);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['id' => $id]);
        $result = $query->first();

        $this->assertInstanceOf('\App\Model\Entity\User', $result);
        $this->assertSame($data['first_name'], $result->get('first_name'));
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $id = '51fed113-bd86-4a7b-886c-92f65a90e8a2';

        $this->delete('/users/' . $id);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['id' => $id]);
        $this->assertEquals(0, $query->count());
    }
}
