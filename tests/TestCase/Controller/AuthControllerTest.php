<?php

namespace App\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\AuthController Test Case
 */
class AuthControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);
    }

    /**
     * Test login success method
     *
     * @return void
     */
    public function testLoginSuccess()
    {
        $credential = [
            'email' => 'admin@dev.localhost',
            'password' => 'admin'
        ];

        $this->post('/auth/login', $credential);

        $this->assertResponseSuccess();
        $this->assertResponseContains('success');

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['email' => $credential['email']]);
        /* @var \App\Model\Entity\User $identity */
        $identity = $query->first();

        $this->assertFalse($identity->tokenExpired());
    }

    /**
     * Test login fail because user is inactive  method
     *
     * @return void
     */
    public function testLoginFail()
    {
        $credential = [
            'email' => 'staff@dev.localhost',
            'password' => 'tester'
        ];

        $this->post('/auth/login', $credential);
        $this->assertResponseError();
        $this->assertResponseCode(401);
    }

    /**
     * Test register method
     *
     * @return void
     */
    public function testRegister()
    {
        $data = [
            'email' => 'new_user@dev.localhost',
            'password' => 'new_user',
            'first_name' => 'New',
            'last_name' => 'User'
        ];

        $this->post('/auth/register', $data);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['email' => $data['email']]);
        /* @var \App\Model\Entity\User $newUser */
        $newUser = $query->first();

        $this->assertInstanceOf('\App\Model\Entity\User', $newUser);
        $this->assertFalse($newUser->active);
        $this->assertNotEmpty($newUser->activation_secret);
        $this->assertNull($newUser->activation_date);
    }

    /**
     * Test verifyAccount method
     *
     * @return void
     */
    public function testAccountAlreadyVerified()
    {
        $users = TableRegistry::get('Users');
        $email = 'staff@dev.localhost';

        /* @var \App\Model\Entity\User $unvalidatedAccount */
        $unvalidatedAccount = $users->find()->where(['email' => $email])->first();
        $id = $unvalidatedAccount->id;
        $validationSecret = 'f5e94ff6da9a002e25e30ac1f611ffa2';

        $this->get('/auth/verify_account/' . $id . '?check=' . $validationSecret);
        $this->assertResponseSuccess();
        $this->assertResponseContains(__('User is already verified'));
    }

    /**
     * Test verifyAccount method
     *
     * @return void
     */
    public function testAccountVerificationSuccess()
    {
        $users = TableRegistry::get('Users');
        $email = 'unactived_user@dev.localhost';

        /* @var \App\Model\Entity\User $unvalidatedAccount */
        $unvalidatedAccount = $users->find()->where(['email' => $email])->first();
        $id = $unvalidatedAccount->id;
        $validationSecret = $unvalidatedAccount->activation_secret;

        $this->get('/auth/verify_account/' . $id . '?check=' . $validationSecret);
        $this->assertResponseSuccess();
        $this->assertResponseContains(__('User verified successfully'));

        /* @var \App\Model\Entity\User $validatedAccount */
        $validatedAccount = $users->find()->where(['email' => $email])->first();

        $this->assertTrue($validatedAccount->active);
        $this->assertNull($validatedAccount->activation_secret);
        $this->assertNotEmpty($validatedAccount->activation_date);
    }

    /**
     * Test verifyAccount method
     *
     * @return void
     */
    public function testAccountVerificationFails()
    {
        $id = '8765579a-e861-4fda-8045-3fa3b95abc82';
        $wrongSecret = 'wrongValue';

        $this->get('/auth/verify_account/' . $id . '?check=' . $wrongSecret);
        $this->assertResponseFailure();
        $this->assertResponseContains(__('Failed to verify token'));
    }

    /**
     * Test verifyAccountMissingSecret method
     *
     * @return void
     */
    public function testAccountMissingSecret()
    {
        $id = '8765579a-e861-4fda-8045-3fa3b95abc82';
        $this->get('/auth/verify_account/' . $id);
        $this->assertResponseError();
        $this->assertResponseContains(__('Missing validation token'));
    }

    /**
     * Test resetPassword method
     *
     * @return void
     */
    public function testResetPassword()
    {
        $data = ['email' => 'staff@dev.localhost'];

        $this->post('/auth/reset_password/', $data);
        $this->assertResponseOk();
        $this->assertResponseContains(__('Reset password token sent to your email'));

        $users = TableRegistry::get('Users');
        /* @var \App\Model\Entity\User $user */
        $user = $users->find()->where($data)->first();

        $this->assertNotEmpty($user->reset_password_secret);
    }

    /**
     * Test resetPassword failure method
     *
     * @return void
     */
    public function testResetPasswordFailure()
    {
        $data = ['email' => 'unkown_email@dev.localhost'];

        $this->post('/auth/reset_password/', $data);
        $this->assertResponseFailure();
        $this->assertResponseContains(__('Unknow email'));
    }

    /**
     * Test resetPassword failure method
     *
     * @return void
     */
    public function testResetPasswordMissingEmail()
    {
        $data = ['email' => 'unkown_email@dev.localhost'];

        $this->post('/auth/reset_password/', []);
        $this->assertResponseError();
        $this->assertResponseContains(__('Missing email'));
    }

    /**
     * Test changePassword successfully method
     *
     * @return void
     */
    public function testChangePasswordSuccess()
    {
        $id = '51fed113-bd86-4a7b-886c-92f65a90e8a2';
        $check = '726db601b6e91146af977a0a151f9ffc';
        $data = [
            'email' => 'staff@dev.localhost',
            'password' => 'new_password'
        ];

        $this->post('/auth/change_password/' . $id . '?check=' . $check, $data);
        $this->assertResponseOk();

        $users = TableRegistry::get('Users');
        /* @var \App\Model\Entity\User $user */
        $user = $users->find()->where(['email' => $data['email']])->first();

        $this->assertTrue($user->checkPassword($data['password']));
    }

    /**
     * Test changePassword failure method
     *
     * @return void
     */
    public function testChangePasswordFail()
    {
        $id = '51fed113-bd86-4a7b-886c-92f65a90e8a2';
        $check = 'wrong_secret';

        $this->post('/auth/change_password/' . $id . '?check=' . $check, []);
        $this->assertResponseFailure();
        $this->assertResponseContains(__('Failed to reset password'));
    }

    /**
     * Test changePassword failure for missing secret token method
     *
     * @return void
     */
    public function testChangePasswordMissingSecret()
    {
        $id = '51fed113-bd86-4a7b-886c-92f65a90e8a2';

        $this->post('/auth/change_password/' . $id, []);
        $this->assertResponseError();
        $this->assertResponseContains(__('Missing validation token'));
    }
}
