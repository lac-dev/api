<?php
use Migrations\AbstractMigration;

/**
 * Class CreateUsers
 */
class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users', ['id' => false, 'primary_key' => ['id']]);

        $table
            ->addColumn('id', 'uuid', [
                'null' => false,
            ])
            ->addColumn('email', 'text', [
                'null' => false,
            ])
            ->addColumn('password', 'text', [
                'null' => false,
            ])
            ->addColumn('first_name', 'text', [
                'null' => false,
            ])
            ->addColumn('last_name', 'text', [
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->addColumn('is_superuser', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->addColumn('role', 'text', [
                'default' => 'user',
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('deleted', 'timestamp', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('settings', 'json', [
                'null' => true,
            ])
            ->addColumn('token', 'text', [
                'null' => true,
            ])
            ->addColumn('token_expires', 'datetime', [
                'null' => true,
            ])
            ->addColumn('api_token', 'text', [
                'null' => true,
            ])
            ->addColumn('tos_date', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('activation_secret', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('activation_date', 'datetime', [
                'null' => true,
            ])
            ->addColumn('reset_password_secret', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addIndex(['email'], ['unique' => true]);

        $table->create();
    }
}
